/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.enitity;

/**
 *
 * @author Shuuyu
 */
public class Book {
    private int MaSach;
    private String Tensach;
    private String MaLoai;
    private String Tomtat;
    private String MaTG;
    private String MaNXB;
    
    public Book(int MaSach, String TenSach, String MaLoai, String Tomtat, String MaTG, String MaNXB) {
        
    }
    public int getMaSach(){
        return MaSach;
    }
    public void setMaSach(int MaSach){
        this.MaSach = MaSach;
    }
     public String getTensach(){
        return Tensach;
    }
    public void setTensach(String Tensach){
        this.Tensach = Tensach;
    }
    public String getMaloai(){
        return MaLoai;
    }
    public void setMaLoai(String MaLoai){
        this.MaLoai = MaLoai;
    }
    public String getTomtat(){
        return Tomtat;
    }
    public void setTomtat(String Tomtat){
        this.Tomtat = Tomtat;
    }
    public String getMaTG(){
        return MaTG;
    }
    public void setMaTG(String MaTG){
        this.MaTG = MaTG;
    }
    public String getMaNXB(){
        return MaNXB;
    }
    public void setMaNXB(String MaNXB){
        this.MaNXB = MaNXB;
    }
    
    @Override
    public String toString(){
        return "Book{" + "MaSach" + MaSach + ",Tensach" + Tensach + ",MaLoai" + MaLoai + ",Tomtat" + Tomtat + ",MaTG" + MaTG + ",MaNXB" + MaNXB + '}';
    }
    @Override 
    public boolean equals (Object obj){
        if (obj == null || !(obj instanceof Book)){
            return false;
        }
        Book other = (Book) obj;
        return this.MaSach == other.MaSach;
    }
    
    @Override
    public int hashCode(){
        int hash = 5;
        hash = 11 * hash + this.MaSach;
        return hash;
    }
}
